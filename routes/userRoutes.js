
// Dependencies
const express = require("express");
const auth = require("../auth.js");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");


// All Routers
// Registration
router.post("/register", userControllers.registerUser);
// Login
router.post("/login", userControllers.loginUser);
// Check Email
router.post("/checkEmail", userControllers.checkEmailExists);
// Check user details
router.get("/details", auth.verify, userControllers.getUserDetails);
// Check user orders
router.get("/myOrders", auth.verify, userControllers.getUserOrders);

router.delete("/deleteOrders", auth.verify, userControllers.deleteOrders);



module.exports = router;