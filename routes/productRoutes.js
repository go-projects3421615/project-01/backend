
// Dependencies
const express = require("express");
const auth = require("../auth.js");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js");


// Add Product
router.post("/addProduct", auth.verify, productControllers.addProduct);
// Get all active and inactive products
router.get("/all", productControllers.allProducts);
// Get single product
router.get("/:productId", productControllers.searchProduct);
// Update single product
router.put("/:productId/update", auth.verify, productControllers.updateProduct);

router.delete("/deleteOrders/:productId", auth.verify, productControllers.deleteOrders);



module.exports = router;