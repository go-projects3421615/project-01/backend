
// Dependencies
const express = require("express");
const auth = require("../auth.js");
const router = express.Router();
const cartControllers = require("../controllers/cartControllers.js");
const Products = require("../models/Products.js");

// All Routers

router.get("/cart", auth.verify, cartControllers.checkCart);

router.post("/addToCart", auth.verify, cartControllers.addToCart);

router.delete("/deleteItems", auth.verify, cartControllers.deleteCartItems);

router.get("/cartItem/:index", auth.verify, cartControllers.checkOneItem);

router.post("/checkout", auth.verify, cartControllers.checkoutCartItems);

module.exports = router;