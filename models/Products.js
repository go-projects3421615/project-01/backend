
// Dependencies
const mongoose = require("mongoose");


// Model Schema
const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	orders: [
		{
            userId: {
				type: String,
				required: [true, "UserId is required"]
			},
            userEmail: {
				type: String,
				required: [true, "UserEmail is required"]
			},
			quantity: {
				type: Number,
				default: 0
			},
            subTotal: {
				type: Number,
				default: 0
			}
		}		
    ]
});

module.exports = mongoose.model("Products", productSchema);
