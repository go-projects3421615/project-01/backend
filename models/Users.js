
// Dependencies
const mongoose = require("mongoose");


// Model Schema
const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
    isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			productName: {
				type: String,
				required: [true, "Product name is required"]
			},
			quantity: {
				type: Number,
				default: 0
			},
			subTotal: {
				type: Number,
				default: 0
			}
		}
	]
});
module.exports = mongoose.model("Users", userSchema);

