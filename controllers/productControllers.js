
// Dependencies
const Products = require("../models/Products");
const auth = require("../auth.js");


// Creating or adding a product for admin only
module.exports.addProduct = (request, response) => {
	const hashedPassword = auth.decode(request.headers.authorization);
	let newProduct = new Products({
		name: request.body.name,
		price: request.body.price
	});
	if(hashedPassword.isAdmin){
		return newProduct.save()
		.then(product => {
			console.log(product);
			response.send(product);
		})
		.catch(error => {
			console.log(error);
			response.send(false);
		});
	}
	else {
		return response.status(401).send("User must be ADMIN to access this functionality");
	}
}

// Get all products
module.exports.allProducts = (request, response) => {
	return Products.find({})
	.then(result => response.send(result))
	.catch(error => response.send(error));
}

// Get specific product
module.exports.searchProduct = (request, response) => {
	return Products.findById(request.params.productId)
	.then(result => {
		response.send(result);
	})
	.catch(error => {
		response.send(error);
	})
}

// Update Product
module.exports.updateProduct = (request, response) =>{
	const hashedPassword = auth.decode(request.headers.authorization);
	if(hashedPassword.isAdmin){
		let updatedProductDetails = {
			name: request.body.name,
			price: request.body.price
		}
		return Products.findByIdAndUpdate(request.params.productId, updatedProductDetails, {new:true})
		.then(result =>{
			console.log(result);
			response.send(result);
		})
		.catch(error =>{
			console.log(error);
			response.send(false);
		});
	}
	else{
		return response.send(false);
	}
}



// Delete/Complete ordered products
module.exports.deleteOrders = (request, response) => {
	const hashedPassword = auth.decode(request.headers.authorization);
	if(hashedPassword.isAdmin){
		return Products.findById(request.params.productId)
		.then(result => {
			result.orders.splice(0, result.orders.length);
			result.save();
			response.send(result);
		})
		.catch(error => response.send(error));
	}
	else{
		return response.send(false);
	}
}