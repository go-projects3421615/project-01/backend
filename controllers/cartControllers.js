
// Dependencies for password token
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// Dependencies for database models
const Users = require("../models/Users.js");
const Products = require("../models/Products.js");
const Carts = require("../models/Carts.js");


// Check Cart
module.exports.checkCart = async (request, response) => {
    const decodedToken = auth.decode(request.headers.authorization);
	const user = Users.findById(decodedToken.id)

    let order = await Carts.find({userId: decodedToken.id})
    .then(result => {
        response.send(result);
    })
}

// Add Product on Cart
module.exports.addToCart = async (request, response) => {
	const decodedToken = auth.decode(request.headers.authorization);
    let productName = await Products.findById(request.body.productId)
    .then(result => result.name);
    let newData = {
        productName: productName
    }
    let productPrice = await Products.findById(request.body.productId)
    .then(result => result.price);
    let priceData = {
        productPrice: productPrice
    }
    console.log(newData)
    console.log(priceData)
    let cart = await Carts.findOne({userId: decodedToken.id})
    .then(result => {
        console.log(result.userId)
        console.log(result.userEmail)
        result.cartProducts.push({
            productId: request.body.productId,
            productName: newData.productName,
            quantity: request.body.quantity,
            subTotal: (priceData.productPrice * request.body.quantity)
        })
        result.cartTotalAmount = result.cartTotalAmount + (priceData.productPrice * request.body.quantity)
        console.log(result)
        return result.save()
        .then(res => {
            console.log(res)
            response.send(true)
        })
        .catch(err => {
            console.log(err)
            response.send(err)
        })
    })
}


// Retrieve specific cart items
module.exports.checkOneItem = async (request, response) => {
    const decodedToken = auth.decode(request.headers.authorization);
	const user = Users.findById(decodedToken.id)

    let order = await Carts.findOne({userId: decodedToken.id})
    .then(result => {
        if(result.cartProducts.length <= 0){
            response.send({Message: "Empty Cart"})
        }
        else{
            if(request.params.index > -1 && request.params.index < result.cartProducts.length){
                console.log(result.cartProducts[request.params.index].productId);
                response.send(result.cartProducts[request.params.index]);
            }
            else{
                response.send({Message: "Invalid index"});
            }
        }
    })
    .catch(error => {
        response.send(false);
    })
}


// Checkout Cart
module.exports.checkoutCartItems = async (request, response) => {
    const decodedToken = auth.decode(request.headers.authorization);
    let cartOwner = await Carts.findOne({userId: decodedToken.id})
    .then(result => {
        if(result.cartProducts.length <= 0){
            response.send({Message: "Empty Cart"});
        }
        else{
            for(i = 0; i < result.cartProducts.length; i++){
                let itemId = result.cartProducts[i].productId;
                let itemName = result.cartProducts[i].productName;
                let itemQuantity = result.cartProducts[i].quantity;
                let itemSubTotal = result.cartProducts[i].subTotal;
                const isProductUpdated = Products.findById(result.cartProducts[i].productId)
                .then(product => {
                    product.orders.push({
                        userId: result.userId,
                        userEmail: result.userEmail,
                        quantity: itemQuantity,
                        subTotal: itemSubTotal
                    })
                    return product.save()
                    .then(result => {
                        console.log(result);
                        return true;
                    })
                    .catch(error => {
                        console.log(error);
                        return false;
                    })
                })
                const isUserUpdated = Users.findById(decodedToken.id)
                .then(user => {
                    user.orders.push({
                        productId: itemId,
                        productName: itemName,
                        quantity: itemQuantity,
                        subTotal: itemSubTotal
                    })
                    return user.save()
                    .then(result => {
                        console.log(result);
                        return true;
                    })
                    .catch(error => {
                        response.send(error);
                        return false;
                    });
                })
            }
            result.cartProducts.splice(0, result.cartProducts.length);
            result.cartTotalAmount = 0;
            result.save();
            response.send({Message: "Order Successfully Placed"});
        }
    })
    .catch(error => response.send(error));
}


// Delete Cart Items
module.exports.deleteCartItems = async (request, response) => {
    const decodedToken = auth.decode(request.headers.authorization);
    let cartOwner = await Carts.findOne({userId: decodedToken.id})
    .then(result => {
        result.cartProducts.splice(0, result.cartProducts.length);
        result.cartTotalAmount = 0;
        result.save();
        response.send(result);
    })
    .catch(error => {
        console.log(error);
        response.send(error);
    })
}
