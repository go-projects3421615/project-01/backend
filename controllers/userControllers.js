
// Dependencies for password token
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// Dependencies for database models
const Users = require("../models/Users.js");
const Products = require("../models/Products.js");
const Carts = require("../models/Carts.js");


// Register User
module.exports.registerUser = (request, response) => {
	Users.findOne({email: request.body.email})
    .then(result => {
        if(result !== null && result.email === request.body.email){
            console.log("Account creation failed. Email already exist in the database");
            return response.status(400).send({error: "Email already exist"});
        }
        else{
            let newUser = new Users({
                email: request.body.email,
                password: bcrypt.hashSync(request.body.password, 10)
            })
            return newUser.save()
            .then(user => {
                let newCart = new Carts({
                    userId: user._id,
                    userEmail: user.email
                })
                return newCart.save()
                .then(cart => {
                    console.log(user);
                    response.send(true);
                })
            })
            .catch(error =>{
                console.log(error);
                response.send(false);
            })
	    }
	})
}
// User Login
module.exports.loginUser = (request, response) => {
    Users.findOne({email: request.body.email})
    .then(result => {
        if(result != null && result.email == request.body.email){
            const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
            if(isPasswordCorrect){
                console.log("Login successful. Access token created");
                return response.send({accessToken: auth.createAccessToken(result)});
            }
            else{
                console.log("Login failed. Incorrect password");
                return response.send(false);
            }
        }
        else{
            console.log("Login failed. User not found");
            return response.send(false);
        }
    })
    .catch(error => response.send(error));
}

// Check if email exist
module.exports.checkEmailExists = (request, response) => {
    return Users.find({email: request.body.email})
    .then(result => {
        if(result.length > 0){
            return response.send(true);
        }
        else{
            return response.send(false);
        }
    })
    .catch(error => response.send(error));
}


// Retrieve user details
module.exports.getUserDetails = (request, response) => {
	const decodedToken = auth.decode(request.headers.authorization);
	return Users.findById(decodedToken.id)
	.then(result => {
		result.password = "******";
        console.log(result);
		response.send(result);
	})
	.catch(error => response.send(error));
}

// Retrieve user orders
module.exports.getUserOrders = (request, response) => {
	const decodedToken = auth.decode(request.headers.authorization);
	return Users.findById(decodedToken.id)
	.then(result => {
		response.send(result.orders);
	})
	.catch(error => response.send(error));
}

module.exports.deleteOrders = async (request, response) => {
    const decodedToken = auth.decode(request.headers.authorization);
    return Users.findById(decodedToken.id)
    .then(result => {
        result.orders.splice(0, result.orders.length);
        result.save();
        response.send(result);
    })
    .catch(error => {
        console.log(error);
        response.send(error);
    })
}